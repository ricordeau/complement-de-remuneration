import { NgModule } from '@angular/core';

import { ComplementRemunerationComponent } from './complement-remuneration.component';
import { SharedModule } from '../shared/shared.module';
import { OngletPublicationsModule } from './onglets/onglet-publications/onglet-publications.module';
import { OngletSuiviDesSitesModule } from './onglets/onglet-suivi-des-sites/onglet-suivi-des-sites.module';

@NgModule({
  declarations: [ComplementRemunerationComponent],
  imports: [
    OngletPublicationsModule,
    OngletSuiviDesSitesModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [ComplementRemunerationComponent]
})
export class ComplementRemunerationModule { }
