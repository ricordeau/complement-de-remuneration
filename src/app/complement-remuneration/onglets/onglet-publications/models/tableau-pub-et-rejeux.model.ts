import { StatutPublicationEnum } from './publication.model';

export class FiltreStatut {
  statut: StatutPublicationEnum;
  nbOccurrences: number;
  isChecked: boolean;

  constructor(statut: StatutPublicationEnum, nbOccurrences: number, isChecked: boolean) {
    this.statut = statut;
    this.nbOccurrences = nbOccurrences;
    this.isChecked = isChecked;
  }

  getLabelFiltre(): string {
    const statutLbl = this.statut === StatutPublicationEnum.ECO ? 'En cours de calcul' : this.statut;
    return statutLbl + ' (' + this.nbOccurrences + ')';
  }
}
