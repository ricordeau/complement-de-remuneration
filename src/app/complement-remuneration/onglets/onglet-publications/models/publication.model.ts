export class Publication {
  id: string;
  statut: StatutPublicationEnum;
  type: TypePublicationEnum;
  periode: string;
  receptionFichierPeriode: string;
  datePublication: string;
  nbSitesEnErreur: number;
  commUser: string;
}

export enum StatutPublicationEnum {
  ECO = 'En cours',
  ERR = 'En erreur',
  AVA = 'A valider',
  VAL = 'Validée',
  PUB = 'Publiée'
}

export enum TypePublicationEnum {
  PON = 'Ponctuelle',
  REJ = 'Rejeu',
  ANN = 'Annuelle',
  NOM = 'Nominale',
  CRB = 'Courbes'
}
