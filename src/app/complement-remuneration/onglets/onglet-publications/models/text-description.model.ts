export const TEXT_DESCRIPTION = 'Le complément de rémunération a été introduit par la LTECV. Dans ce mécanisme où les producteurs ' +
  'd\'électricité à partir d\'énergie renouvelable commercialisent leur énergie directement sur les marchés, une prime vient compenser ' +
  'l\'écart entre les revenus tirés de cette vente et un niveau de rémunération de référence, fixé selon le type d\'installations par la ' +
  'puissance publique dans le cadre d\'un arrêté tarifaire ou par le producteur dans le cadre d\'une procédure de mise en concurrence. ' +
  'Ce complément de rémunération ' + 'peut généralement être qualifié de prime variable, ou ex post, dans la mesure où son montant ' +
  's\'ajuste pour compenser la différence ' + 'entre la rémunération de référence et un revenu marché de référence.\nCe dispositif vise ' +
  'à exposer les producteurs aux signaux des ' + 'prix de marché de court terme, tout en leur garantissant une rémunération raisonnable. ' +
  'Ce dispositif est prévu aux articles L.314-18 ' + 'à L.314-27 du code de l\'énergie.\nLa France a fait évoluer ses dispositifs de ' +
  'soutien afin de se conformer aux lignes directrices ' + 'européennes, imposant de recourir à des mécanismes de rémunération sur le ' +
  'marché avec prime pour les installations de puissance ' + 'supérieure à 500 kW, ou 3 MW ou 3 unités de production pour la filière ' +
  'éolienne, à partir du 1er Janvier 2016. Depuis l\'adoption ' + 'du décret relatif au complément de rémunération au printemps 2016, ' +
  'sur lequel la CRE a rendu un avis, les conditions de celui-ci ont ' + 'été déclinées par filière dans différents arrêtés tarifaires ' +
  'et appels d\'offres.';

export enum MoreOrLessTextEnum {
  MORE = 'Afficher plus...',
  LESS = 'Afficher moins...'
}
