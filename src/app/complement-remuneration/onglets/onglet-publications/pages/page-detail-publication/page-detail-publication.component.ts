import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-detail-publication',
  templateUrl: './page-detail-publication.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageDetailPublicationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
