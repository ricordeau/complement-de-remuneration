import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDetailPublicationComponent } from './page-detail-publication.component';

describe('PageDetailPublicationComponent', () => {
  let component: PageDetailPublicationComponent;
  let fixture: ComponentFixture<PageDetailPublicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageDetailPublicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDetailPublicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(component).toBeTruthy();
  });
});
