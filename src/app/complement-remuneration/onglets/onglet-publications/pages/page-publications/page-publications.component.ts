import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { SiteStat } from '../../../../../shared/models/site-stat.model';
import { PagePublicationsService } from '../../services/page-publications.service';
import { Observable } from 'rxjs';
import { Publication } from '../../models/publication.model';

@Component({
  selector: 'app-page-publications',
  templateUrl: './page-publications.component.html',
  styleUrls: ['./page-publications.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PagePublicationsComponent implements OnInit {

  sitesStats$: Observable<Array<SiteStat>>;

  publications$: Observable<Array<Publication>>;

  constructor(private pagePublicationService: PagePublicationsService) { }

  ngOnInit(): void {
    this.sitesStats$ = this.pagePublicationService.getSitesStats();
    this.publications$ = this.pagePublicationService.getPublicationsEtRejeux();
  }

}
