import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagePublicationsComponent } from './page-publications.component';
import { By } from '@angular/platform-browser';
import { PagePublicationsService } from '../../services/page-publications.service';
import { PUBLICATIONS_STUB, SITE_STATS_PAGE_PUB_STUB } from '../../stubs/publications.stub';
import { of } from 'rxjs';
import { OngletPublicationsModule } from '../../onglet-publications.module';

describe('PagePublicationsComponent', () => {
  let pagePublicationsComponent: PagePublicationsComponent;
  let fixture: ComponentFixture<PagePublicationsComponent>;
  let pagePublicationsServiceSpy: jasmine.SpyObj<PagePublicationsService>;

  beforeEach(() => {
    pagePublicationsServiceSpy = jasmine.createSpyObj(['getSitesStats', 'getPublicationsEtRejeux']);
    pagePublicationsServiceSpy.getSitesStats.and.returnValue(of(SITE_STATS_PAGE_PUB_STUB));
    pagePublicationsServiceSpy.getPublicationsEtRejeux.and.returnValue(of(PUBLICATIONS_STUB));
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ OngletPublicationsModule ],
      providers: [
        { provide: PagePublicationsService, useValue: pagePublicationsServiceSpy }
      ]
    });
    fixture = TestBed.createComponent(PagePublicationsComponent);
    pagePublicationsComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(pagePublicationsComponent).toBeTruthy();
  });

  it('Le titre de la page est \'Complément de rémunération\'', () => {
    const titreElement = fixture.debugElement.query(By.css('h1'));
    expect(titreElement.nativeElement.innerText).toEqual('Complément de rémunération');
  });

  it('Le texte de description est présent', () => {
    const textDescription = fixture.debugElement.nativeElement.querySelector('app-text-description');
    expect(textDescription).toBeDefined();
  });

  it('Il existe 3 statistiques encadrées', () => {
    const statsCadres = fixture.debugElement.nativeElement.querySelectorAll('app-site-stat-cadre');
    expect(statsCadres.length).toEqual(3);
  });

  it('Le bouton \'Lancer le calcul\' est présent', () => {
    const boutonLancerCalcul = fixture.debugElement.nativeElement.querySelector('button');
    expect(boutonLancerCalcul.innerText).toEqual('LANCER LE CALCUL');
  });

  it('Le tableau \'Publications et rejeux\' est présent', () => {
    const tableauPubEtRej = fixture.debugElement.nativeElement.querySelector('app-tableau-pub-et-rejeux');
    expect(tableauPubEtRej).toBeDefined();
  });

});
