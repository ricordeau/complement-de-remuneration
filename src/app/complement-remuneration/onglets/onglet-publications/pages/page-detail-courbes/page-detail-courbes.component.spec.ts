import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDetailCourbesComponent } from './page-detail-courbes.component';

describe('PageDetailCourbesComponent', () => {
  let component: PageDetailCourbesComponent;
  let fixture: ComponentFixture<PageDetailCourbesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageDetailCourbesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDetailCourbesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(component).toBeTruthy();
  });
});
