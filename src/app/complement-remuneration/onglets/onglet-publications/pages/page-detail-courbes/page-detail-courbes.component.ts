import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-detail-courbes',
  templateUrl: './page-detail-courbes.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageDetailCourbesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
