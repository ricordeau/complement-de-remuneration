import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Publication, StatutPublicationEnum, TypePublicationEnum } from '../../models/publication.model';
import { FiltreStatut } from '../../models/tableau-pub-et-rejeux.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tableau-pub-et-rejeux',
  templateUrl: './tableau-pub-et-rejeux.component.html',
  styleUrls: ['./tableau-pub-et-rejeux.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableauPubEtRejeuxComponent implements OnInit {

  @Input()
  publications: Array<Publication> = [];

  displayedColumns: Array<string> = ['id', 'statut', 'type', 'periode', 'receptionFichierPeriode', 'datePublication', 'nbSitesEnErreur', 'commUser'];

  dataSource: MatTableDataSource<Publication>;

  filtresStatut: Array<FiltreStatut> = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  classRow: 'row-highlighted' | '' = 'row-highlighted';

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.initFiltresStatut();
    this.initDataSource();
  }

  initFiltresStatut(): void {
    this.filtresStatut = [
      new FiltreStatut(StatutPublicationEnum.ECO, 0, true),
      new FiltreStatut(StatutPublicationEnum.ERR, 0, true),
      new FiltreStatut(StatutPublicationEnum.AVA, 0, true),
      new FiltreStatut(StatutPublicationEnum.VAL, 0, false),
      new FiltreStatut(StatutPublicationEnum.PUB, 0, false),
    ];

    this.publications.forEach(pub => {
      const filtre: FiltreStatut = this.filtresStatut.find(fs => fs.statut === pub.statut);
      filtre.nbOccurrences++;
    });
  }

  initDataSource(): void {
    this.dataSource = new MatTableDataSource<Publication>(this.publications);
    this.dataSource.filterPredicate = (pub: Publication, filterValue: string) => {
      const filtre: FiltreStatut = this.filtresStatut.find(fs => fs.statut === pub.statut);
      return filtre.isChecked === JSON.parse(filterValue);
    };
    this.dataSource.paginator = this.paginator;
    this.applyFilters();
  }

  applyFilters(): void {
    this.dataSource.filter = 'true';
  }

  navigateToDetail(pub: Publication): void {
    if (this.classRow === 'row-highlighted') {
      const url = pub.type === TypePublicationEnum.CRB ? 'publications/detail-courbes' : 'publications/detail';
      this.router.navigate([url, pub.id]).then();
    }
  }

}
