import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauPubEtRejeuxComponent } from './tableau-pub-et-rejeux.component';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../../../../../shared/routes/complement-remuneration-routing.module';
import { CercleStatutPublicationComponent } from '../cercle-statut-publication/cercle-statut-publication.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import { PUBLICATIONS_STUB } from '../../stubs/publications.stub';
import { TiretIfNullPipe } from '../../../../../shared/pipes/tiret-if-null.pipe';
import { CommentaireComponent } from '../../../../../shared/components/commentaire/commentaire.component';
import { FiltreStatut } from '../../models/tableau-pub-et-rejeux.model';
import { Publication, StatutPublicationEnum, TypePublicationEnum } from '../../models/publication.model';
import { By } from '@angular/platform-browser';

describe('TableauPubEtRejeuxComponent', () => {
  let tableauPubEtRejeuxComponent: TableauPubEtRejeuxComponent;
  let fixture: ComponentFixture<TableauPubEtRejeuxComponent>;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TableauPubEtRejeuxComponent,
        CercleStatutPublicationComponent,
        TiretIfNullPipe,
        CommentaireComponent
      ],
      imports: [
        RouterTestingModule.withRoutes(routes),
        MatCheckboxModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule
      ]
    });
    fixture = TestBed.createComponent(TableauPubEtRejeuxComponent);
    tableauPubEtRejeuxComponent = fixture.componentInstance;
    tableauPubEtRejeuxComponent.publications = PUBLICATIONS_STUB;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(tableauPubEtRejeuxComponent).toBeTruthy();
  });

  it('Je peux appliquer des filtres sur les publications', () => {
    // given
    tableauPubEtRejeuxComponent.filtresStatut = [
      new FiltreStatut(StatutPublicationEnum.ECO, 0, false),
      new FiltreStatut(StatutPublicationEnum.ERR, 0, true),
      new FiltreStatut(StatutPublicationEnum.AVA, 0, false),
      new FiltreStatut(StatutPublicationEnum.VAL, 0, false),
      new FiltreStatut(StatutPublicationEnum.PUB, 0, false),
    ];

    // when
    tableauPubEtRejeuxComponent.applyFilters();
    fixture.detectChanges();

    // then
    const rows = fixture.debugElement.queryAll(By.css('.mat-row'));
    expect(rows.length).toEqual(1);
    const statutPublicationCell = fixture.debugElement.query(By.css('.label-statut'));
    expect(statutPublicationCell.nativeElement.innerText).toEqual('En erreur');
  });

  it('Le nombre d\'élément par statut est affiché à droite du label des checkboxes', () => {
    const labelsCheckboxes = fixture.debugElement.queryAll(By.css('.mat-checkbox-label')).map(lab => lab.nativeElement.innerText);
    expect(labelsCheckboxes).toContain('En cours de calcul (1)');
    expect(labelsCheckboxes).toContain('En erreur (1)');
    expect(labelsCheckboxes).toContain('A valider (3)');
    expect(labelsCheckboxes).toContain('Validée (1)');
    expect(labelsCheckboxes).toContain('Publiée (3)');
  });

  it('On navigue vers la page de détail des courbes d\'une publication lorsque l\'utilisateur clique sur une ligne de type courbe', () => {
    // given
    const publicationTypeCourbe: Publication = PUBLICATIONS_STUB.filter(pub => pub.type === TypePublicationEnum.CRB)[0];
    const routerNavigationSpy = spyOn(router, 'navigate');
    routerNavigationSpy.and.returnValue(new Promise(() => {
      return true;
    }));

    // when
    const ligneTableauTypeCourbe = fixture.debugElement.queryAll(By.css('.mat-row'))
      .filter(row => row.query(By.css('.mat-column-type')).nativeElement.innerText === TypePublicationEnum.CRB)[0];
    ligneTableauTypeCourbe.triggerEventHandler('mousedown', publicationTypeCourbe);
    fixture.detectChanges();

    // then
    expect(routerNavigationSpy).toHaveBeenCalledWith(['publications/detail-courbes', 'PUB 0009']);
  });

  it('On navigue vers la page de détail d\'une publication lorsque l\'utilisateur clique sur une ligne qui n\'est pas ' +
    'de type courbe', () => {
    // given
    const publicationTypeRej: Publication = PUBLICATIONS_STUB.filter(pub => pub.type === TypePublicationEnum.REJ)[0];
    const routerNavigationSpy = spyOn(router, 'navigate');
    routerNavigationSpy.and.returnValue(new Promise(() => {
      return true;
    }));

    // when
    const ligneTableauTypeRej = fixture.debugElement.queryAll(By.css('.mat-row'))
      .filter(row => row.query(By.css('.mat-column-type')).nativeElement.innerText === TypePublicationEnum.REJ)[0];
    ligneTableauTypeRej.triggerEventHandler('mousedown', publicationTypeRej);
    fixture.detectChanges();

    // then
    expect(routerNavigationSpy).toHaveBeenCalledWith(['publications/detail', 'PUB 0007']);
  });

  it('Il n\'y a pas de navigation si l\'utilisateur clique dans la colonne commentaire', () => {
    // given
    const routerNavigationSpy = spyOn(router, 'navigate');
    expect(tableauPubEtRejeuxComponent.classRow).toEqual('row-highlighted');
    const cellComment = fixture.debugElement.query(By.css('td.mat-column-commUser'));
    cellComment.triggerEventHandler('mouseenter', null);
    expect(tableauPubEtRejeuxComponent.classRow).toEqual('');

    // when
    const ligneTableau = fixture.debugElement.query(By.css('.mat-row'));
    ligneTableau.triggerEventHandler('mousedown', null);

    // then
    expect(routerNavigationSpy).toHaveBeenCalledTimes(0);
  });

});
