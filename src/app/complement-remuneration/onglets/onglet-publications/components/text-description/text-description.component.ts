import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MoreOrLessTextEnum, TEXT_DESCRIPTION } from '../../models/text-description.model';

@Component({
  selector: 'app-text-description',
  templateUrl: './text-description.component.html',
  styleUrls: ['./text-description.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextDescriptionComponent implements OnInit {

  textDescription: string = TEXT_DESCRIPTION;

  moreOrLessText = MoreOrLessTextEnum.MORE;

  constructor() {
  }

  ngOnInit(): void {
  }

  switchMoreOrLessText(): void {
    this.moreOrLessText = this.moreOrLessText === MoreOrLessTextEnum.MORE ? MoreOrLessTextEnum.LESS : MoreOrLessTextEnum.MORE;
  }

}
