import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TextDescriptionComponent } from './text-description.component';
import { By } from '@angular/platform-browser';

describe('TextDescriptionComponent', () => {
  let textDescriptionComponent: TextDescriptionComponent;
  let fixture: ComponentFixture<TextDescriptionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ TextDescriptionComponent ]
    });
    fixture = TestBed.createComponent(TextDescriptionComponent);
    textDescriptionComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(textDescriptionComponent).toBeTruthy();
  });

  it('Le texte \'Afficher plus...\' change lorsque l\'utilisateur clique dessus', () => {
    // given
    const spanBoutonAfficherPlus = fixture.debugElement.query(By.css('.more-less'));

    // when
    spanBoutonAfficherPlus.triggerEventHandler('click', null);
    fixture.detectChanges();

    // then
    expect(spanBoutonAfficherPlus.nativeElement.innerText).toEqual('Afficher moins...');
    spanBoutonAfficherPlus.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(spanBoutonAfficherPlus.nativeElement.innerText).toEqual('Afficher plus...');
  });

  it('La hauteur maximale du span de description est augmentée lorsque l\'utilisateur clique sur \'Afficher plus...\'', waitForAsync(() => {
    // given
    let spanDescription = fixture.debugElement.query(By.css('.text-description'));
    const spanBoutonAfficherPlus = fixture.debugElement.query(By.css('.more-less'));

    // when
    expect(window.getComputedStyle(spanDescription.nativeElement).maxHeight).toEqual('65px');
    spanBoutonAfficherPlus.triggerEventHandler('click', null);
    fixture.detectChanges();

    // then
    setTimeout(() => {
      spanDescription = fixture.debugElement.query(By.css('.text-description'));
      expect(window.getComputedStyle(spanDescription.nativeElement).maxHeight).toEqual('185px');
    }, 500);
  }));

});
