import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CercleStatutPublicationComponent } from './cercle-statut-publication.component';
import { StatutPublicationEnum } from '../../models/publication.model';
import { By } from '@angular/platform-browser';

describe('CercleStatutPublicationComponent', () => {
  let cercleStatutPublicationComponent: CercleStatutPublicationComponent;
  let fixture: ComponentFixture<CercleStatutPublicationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ CercleStatutPublicationComponent ]
    }).compileComponents();
    fixture = TestBed.createComponent(CercleStatutPublicationComponent);
    cercleStatutPublicationComponent = fixture.componentInstance;
    cercleStatutPublicationComponent.radius = '20px';
  });

  it('L\'élément est testable', () => {
    expect(cercleStatutPublicationComponent).toBeTruthy();
  });

  it('Le statut à valider est affiché en rose', () => {
    cercleStatutPublicationComponent.statut = StatutPublicationEnum.AVA;
    fixture.detectChanges();
    const divCercle = fixture.debugElement.query(By.css('div'));
    expect(window.getComputedStyle(divCercle.nativeElement).backgroundColor).toEqual('rgb(253, 227, 233)');
    expect(window.getComputedStyle(divCercle.nativeElement).borderColor).toEqual('rgb(255, 156, 181)');
  });

  it('Le statut en cours est affiché en gris', () => {
    cercleStatutPublicationComponent.statut = StatutPublicationEnum.ECO;
    fixture.detectChanges();
    const divCercle = fixture.debugElement.query(By.css('div'));
    expect(window.getComputedStyle(divCercle.nativeElement).backgroundColor).toEqual('rgb(216, 232, 242)');
    expect(window.getComputedStyle(divCercle.nativeElement).borderColor).toEqual('rgb(113, 175, 212)');
  });

  it('Le statut en erreur est affiché en rouge', () => {
    cercleStatutPublicationComponent.statut = StatutPublicationEnum.ERR;
    fixture.detectChanges();
    const divCercle = fixture.debugElement.query(By.css('div'));
    expect(window.getComputedStyle(divCercle.nativeElement).backgroundColor).toEqual('rgb(251, 219, 215)');
    expect(window.getComputedStyle(divCercle.nativeElement).borderColor).toEqual('rgb(246, 48, 39)');
  });

  it('Le statut publié est affiché en vert', () => {
    cercleStatutPublicationComponent.statut = StatutPublicationEnum.PUB;
    fixture.detectChanges();
    const divCercle = fixture.debugElement.query(By.css('div'));
    expect(window.getComputedStyle(divCercle.nativeElement).backgroundColor).toEqual('rgb(237, 244, 223)');
    expect(window.getComputedStyle(divCercle.nativeElement).borderColor).toEqual('rgb(140, 198, 65)');
  });

  it('Le statut validé est affiché en orange', () => {
    cercleStatutPublicationComponent.statut = StatutPublicationEnum.VAL;
    fixture.detectChanges();
    const divCercle = fixture.debugElement.query(By.css('div'));
    expect(window.getComputedStyle(divCercle.nativeElement).backgroundColor).toEqual('rgb(251, 240, 221)');
    expect(window.getComputedStyle(divCercle.nativeElement).borderColor).toEqual('rgb(255, 207, 70)');
  });

  it('Le rayon du cercle dépend de l\'input radius', () => {
    cercleStatutPublicationComponent.statut = StatutPublicationEnum.VAL;
    fixture.detectChanges();
    const divCercle = fixture.debugElement.query(By.css('div'));
    expect(divCercle.nativeElement.style.width).toEqual('20px');
    expect(divCercle.nativeElement.style.height).toEqual('20px');
    expect(divCercle.nativeElement.style.borderRadius).toEqual('20px');
  });

});
