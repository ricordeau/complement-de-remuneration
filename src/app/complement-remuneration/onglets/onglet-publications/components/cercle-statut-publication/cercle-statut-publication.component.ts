import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { StatutPublicationEnum } from '../../models/publication.model';

@Component({
  selector: 'app-cercle-statut-publication',
  templateUrl: './cercle-statut-publication.component.html',
  styleUrls: ['./cercle-statut-publication.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CercleStatutPublicationComponent implements OnInit {

  @Input() radius: string;

  @Input() statut: StatutPublicationEnum;

  statutClass: string;

  constructor() { }

  ngOnInit(): void {
    this.mapStatutValueToStatutCssClass();
  }

  mapStatutValueToStatutCssClass(): void {
    switch (this.statut) {
      case StatutPublicationEnum.AVA:
        this.statutClass = 'statut-a-valider';
        break;
      case StatutPublicationEnum.ECO:
        this.statutClass = 'statut-en-cours';
        break;
      case StatutPublicationEnum.ERR:
        this.statutClass = 'statut-erreur';
        break;
      case StatutPublicationEnum.PUB:
        this.statutClass = 'statut-publiee';
        break;
      case StatutPublicationEnum.VAL:
        this.statutClass = 'statut-validee';
        break;
    }
  }

}
