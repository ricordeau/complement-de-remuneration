import { Injectable } from '@angular/core';
import { SiteStat } from '../../../../shared/models/site-stat.model';
import { Observable, of } from 'rxjs';
import { Publication } from '../models/publication.model';
import { PUBLICATIONS_STUB, SITE_STATS_PAGE_PUB_STUB } from '../stubs/publications.stub';

@Injectable({
  providedIn: 'root'
})
export class PagePublicationsService {

  constructor() {
  }

  getSitesStats(): Observable<Array<SiteStat>> {
    return of(SITE_STATS_PAGE_PUB_STUB);
  }

  getPublicationsEtRejeux(): Observable<Array<Publication>> {
    return of(PUBLICATIONS_STUB);
  }

}
