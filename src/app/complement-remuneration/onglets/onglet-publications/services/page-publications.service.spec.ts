import { TestBed } from '@angular/core/testing';

import { PagePublicationsService } from './page-publications.service';

describe('PagePublicationsService', () => {
  let service: PagePublicationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PagePublicationsService);
  });

  it('L\'élément est testable', () => {
    expect(service).toBeTruthy();
  });
});
