import {
  Publication,
  StatutPublicationEnum,
  TypePublicationEnum
} from '../models/publication.model';
import { SiteStat } from '../../../../shared/models/site-stat.model';

export const SITE_STATS_PAGE_PUB_STUB: Array<SiteStat> = [
  { value: '94', description: 'Nombre de sites en CR' },
  { value: '+2%', description: 'vs mois précédent' },
  { value: '-4%', description: 'vs total 2019' }
];

export const PUBLICATIONS_STUB: Array<Publication> = [
  {
    id: 'PUB 0009',
    statut: StatutPublicationEnum.AVA,
    type: TypePublicationEnum.CRB,
    periode: '01/2000',
    receptionFichierPeriode: '04/01/2020',
    datePublication: null,
    nbSitesEnErreur: null,
    commUser: null
  },
  {
    id: 'PUB 0008',
    statut: StatutPublicationEnum.ECO,
    type: TypePublicationEnum.PON,
    periode: '01/01/2000',
    receptionFichierPeriode: '02/01/2020',
    datePublication: null,
    nbSitesEnErreur: null,
    commUser: null
  },
  {
    id: 'PUB 0007',
    statut: StatutPublicationEnum.AVA,
    type: TypePublicationEnum.REJ,
    periode: '01/2000',
    receptionFichierPeriode: '02/12/2019',
    datePublication: null,
    nbSitesEnErreur: 16,
    commUser: null
  },
  {
    id: 'PUB 0006',
    statut: StatutPublicationEnum.VAL,
    type: TypePublicationEnum.ANN,
    periode: '2000',
    receptionFichierPeriode: '05/11/2019',
    datePublication: null,
    nbSitesEnErreur: 2,
    commUser: null
  },
  {
    id: 'PUB 0005',
    statut: StatutPublicationEnum.AVA,
    type: TypePublicationEnum.NOM,
    periode: '01/2000',
    receptionFichierPeriode: '04/10/2019',
    datePublication: null,
    nbSitesEnErreur: 0,
    commUser: null
  },
  {
    id: 'PUB 0004',
    statut: StatutPublicationEnum.ERR,
    type: TypePublicationEnum.NOM,
    periode: '01/2000',
    receptionFichierPeriode: '05/09/2019',
    datePublication: '11/09/2019',
    nbSitesEnErreur: 9,
    commUser: null
  },
  {
    id: 'PUB 0003',
    statut: StatutPublicationEnum.PUB,
    type: TypePublicationEnum.PON,
    periode: '01/01/2000',
    receptionFichierPeriode: '05/10/2019',
    datePublication: '05/08/2019',
    nbSitesEnErreur: 22,
    commUser: null
  },
  {
    id: 'PUB 0002',
    statut: StatutPublicationEnum.PUB,
    type: TypePublicationEnum.NOM,
    periode: '01/2000',
    receptionFichierPeriode: '05/10/2019',
    datePublication: '05/08/2019',
    nbSitesEnErreur: 12,
    commUser: null
  },
  {
    id: 'PUB 0001',
    statut: StatutPublicationEnum.PUB,
    type: TypePublicationEnum.NOM,
    periode: '01/2000',
    receptionFichierPeriode: '05/10/2019',
    datePublication: '05/10/2019',
    nbSitesEnErreur: 6,
    commUser: null
  }
];
