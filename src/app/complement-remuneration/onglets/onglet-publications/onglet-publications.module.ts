import { NgModule } from '@angular/core';
import { PagePublicationsComponent } from './pages/page-publications/page-publications.component';
import { SharedModule } from '../../../shared/shared.module';
import { TextDescriptionComponent } from './components/text-description/text-description.component';
import { TableauPubEtRejeuxComponent } from './components/tableau-pub-et-rejeux/tableau-pub-et-rejeux.component';
import { CercleStatutPublicationComponent } from './components/cercle-statut-publication/cercle-statut-publication.component';
import { PageDetailPublicationComponent } from './pages/page-detail-publication/page-detail-publication.component';
import { PageDetailCourbesComponent } from './pages/page-detail-courbes/page-detail-courbes.component';


@NgModule({
  declarations: [
    PagePublicationsComponent,
    PageDetailPublicationComponent,
    PageDetailCourbesComponent,
    TextDescriptionComponent,
    TableauPubEtRejeuxComponent,
    CercleStatutPublicationComponent
  ],
  imports: [SharedModule]
})
export class OngletPublicationsModule {
}
