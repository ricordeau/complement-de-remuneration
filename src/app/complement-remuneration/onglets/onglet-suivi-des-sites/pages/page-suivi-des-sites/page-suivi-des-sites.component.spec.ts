import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSuiviDesSitesComponent } from './page-suivi-des-sites.component';
import { OngletSuiviDesSitesModule } from '../../onglet-suivi-des-sites.module';
import { PageSuiviDesSitesService } from '../../services/page-suivi-des-sites.service';
import { SITE_A_SUIVRE_STUB, SITE_STATS_PAGE_SUIVI_STUB } from '../../stubs/suivi-des-sites.stub';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('PageSuiviDesSitesComponent', () => {
  let pageSuiviDesSitesComponent: PageSuiviDesSitesComponent;
  let fixture: ComponentFixture<PageSuiviDesSitesComponent>;
  let pageSuiviDesSitesServiceSpy: jasmine.SpyObj<PageSuiviDesSitesService>;

  beforeEach(() => {
    pageSuiviDesSitesServiceSpy = jasmine.createSpyObj(['getSitesStats', 'getSitesASuivre']);
    pageSuiviDesSitesServiceSpy.getSitesStats.and.returnValue(of(SITE_STATS_PAGE_SUIVI_STUB));
    pageSuiviDesSitesServiceSpy.getSitesASuivre.and.returnValue(of(SITE_A_SUIVRE_STUB));
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ OngletSuiviDesSitesModule ],
      providers: [
        { provide: PageSuiviDesSitesService, useValue: pageSuiviDesSitesServiceSpy }
      ]
    });
    fixture = TestBed.createComponent(PageSuiviDesSitesComponent);
    pageSuiviDesSitesComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(pageSuiviDesSitesComponent).toBeTruthy();
  });

  it('Le titre de la page est \'Liste des sites\'', () => {
    const titreElement = fixture.debugElement.query(By.css('h1'));
    expect(titreElement.nativeElement.innerText).toEqual('Liste des sites');
  });

  it('Il existe 6 statistiques encadrées', () => {
    const statsCadres = fixture.debugElement.nativeElement.querySelectorAll('app-site-stat-cadre');
    expect(statsCadres.length).toEqual(6);
  });

  it('Un tableau \'SITES À SUIVRE\' existe et comporte 7 lignes', () => {
    const tableauxSites = fixture.debugElement.nativeElement.querySelectorAll('app-tableau-site');
    const tableauSiteASuivre = tableauxSites[0];
    const headerTableauSiteASuivre = tableauSiteASuivre.querySelector('.tableau-header');
    expect(headerTableauSiteASuivre.innerText).toEqual('SITES À SUIVRE');
    const lignesTableauSiteASuivre = tableauSiteASuivre.querySelectorAll('tr.mat-row');
    expect(lignesTableauSiteASuivre.length).toEqual(7);
  });

  it('Un tableau \'SITES RÉSOLUS\' existe et comporte 1 ligne', () => {
    const tableauxSites = fixture.debugElement.nativeElement.querySelectorAll('app-tableau-site');
    const tableauSiteResolu = tableauxSites[1];
    const headerTableauSiteASuivre = tableauSiteResolu.querySelector('.tableau-header');
    expect(headerTableauSiteASuivre.innerText).toEqual('SITES RÉSOLUS');
    const lignesTableauSiteASuivre = tableauSiteResolu.querySelectorAll('tr.mat-row');
    expect(lignesTableauSiteASuivre.length).toEqual(1);
  });

});
