import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SiteStat } from '../../../../../shared/models/site-stat.model';
import { PageSuiviDesSitesService } from '../../services/page-suivi-des-sites.service';
import { Site } from '../../models/site.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-page-suivi-des-sites',
  templateUrl: './page-suivi-des-sites.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageSuiviDesSitesComponent implements OnInit {

  sitesStats$: Observable<Array<SiteStat>>;

  sitesASuivre$: Observable<Array<Site>>;

  sitesResolus$: Observable<Array<Site>>;

  constructor(private pageSuiviDesSitesService: PageSuiviDesSitesService) { }

  ngOnInit(): void {
    this.sitesStats$ = this.pageSuiviDesSitesService.getSitesStats();
    const sites$: Observable<Array<Site>> = this.pageSuiviDesSitesService.getSitesASuivre();
    this.sitesASuivre$ = sites$.pipe(map(sites => sites.filter(s => !s.isResolu)));
    this.sitesResolus$ = sites$.pipe(map(sites => sites.filter(s => s.isResolu)));
  }

}
