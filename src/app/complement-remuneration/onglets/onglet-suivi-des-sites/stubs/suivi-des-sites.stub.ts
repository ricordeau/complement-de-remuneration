import { SiteStat } from '../../../../shared/models/site-stat.model';
import { Site } from '../models/site.model';

export const SITE_STATS_PAGE_SUIVI_STUB: Array<SiteStat> = [
  { value: '7', description: 'Site(s) à suivre' },
  { value: '0', description: 'Site(s) non trouvé(s)' },
  { value: '1', description: 'CDC non trouvé(s)' },
  { value: '2', description: 'Autoconsommation produisant sans conso' },
  { value: '4', description: 'Dépassement' },
  { value: '0', description: 'Erreur paramètre' }
];

export const SITE_A_SUIVRE_STUB: Array<Site> = [
  {
    refContrat: 'BOA0032361',
    idc: '09759851',
    isAutoconso: true,
    nomInstallation: 'LA FENNETRIE 2',
    commClient: 'AUTOC produisant et sans conso',
    commUser: '',
    isResolu: false
  },
  {
    refContrat: 'BOA0032283',
    idc: '16150424',
    isAutoconso: true,
    nomInstallation: 'SOGICA BBJ SAS',
    commClient: 'CDC_NON_TROUVEE',
    commUser: '',
    isResolu: false
  },
  {
    refContrat: 'BOA0030300',
    idc: '23251171',
    isAutoconso: false,
    nomInstallation: 'PARC EOLIEN DE LABRUGUIERE 2',
    commClient: 'DEPASSEMENT',
    commUser: '',
    isResolu: false
  },
  {
    refContrat: 'BOA0032281',
    idc: '16540055',
    isAutoconso: false,
    nomInstallation: 'METHALAYOU',
    commClient: 'AUTOC produisant et sans conso',
    commUser: '',
    isResolu: false
  },
  {
    refContrat: 'BOA0029464',
    idc: '15250424',
    isAutoconso: true,
    nomInstallation: 'F.E. D\'ANTEZANT LA CHAPELLE - LES TERRIERS',
    commClient: 'DEPASSEMENT',
    commUser: '',
    isResolu: false
  },
  {
    refContrat: 'BOA0030110',
    idc: '09350506',
    isAutoconso: false,
    nomInstallation: 'Parc Eolien de Vihiersois 2',
    commClient: 'DEPASSEMENT',
    commUser: '',
    isResolu: false
  },
  {
    refContrat: 'BOA0030236',
    idc: '14542374',
    isAutoconso: false,
    nomInstallation: 'PARC EOLIEN DE CARFORT',
    commClient: 'DEPASSEMENT',
    commUser: '',
    isResolu: false
  },
  {
    refContrat: 'BOA0029281',
    idc: '04343841',
    isAutoconso: true,
    nomInstallation: 'SEPE DE LA BUTTE DE SOIGNY 1',
    commClient: 'DEPASSEMENT',
    commUser: '',
    isResolu: true
  }
];
