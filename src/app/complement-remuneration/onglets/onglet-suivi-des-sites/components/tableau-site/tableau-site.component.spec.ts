import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauSiteComponent } from './tableau-site.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { SITE_A_SUIVRE_STUB } from '../../stubs/suivi-des-sites.stub';
import { CommentaireComponent } from '../../../../../shared/components/commentaire/commentaire.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { By } from '@angular/platform-browser';

describe('TableauSiteComponent', () => {
  let tableauSiteComponent: TableauSiteComponent;
  let fixture: ComponentFixture<TableauSiteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TableauSiteComponent,
        CommentaireComponent
      ],
      imports: [
        MatPaginatorModule,
        MatTableModule,
        MatCheckboxModule,
        MatIconModule
      ]
    });
    fixture = TestBed.createComponent(TableauSiteComponent);
    tableauSiteComponent = fixture.componentInstance;
    tableauSiteComponent.sites = SITE_A_SUIVRE_STUB;
    tableauSiteComponent.titre = 'Test tableau site';
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(tableauSiteComponent).toBeTruthy();
  });

  it('Le titre passé en input s\'affiche dans le header du tableau', () => {
    const divHeader = fixture.debugElement.query(By.css('.tableau-header'));
    expect(divHeader.nativeElement.innerText).toEqual('Test tableau site');
  });

  it('Une icône sunny est affichée lorsque le site est en autoconso', () => {
    const rows = fixture.debugElement.queryAll(By.css('.mat-row'));
    const rowsAutoconso = rows.filter(row => row.query(By.css('.icon-sunny')) !== null);
    const nomsInstallAutoconso = rowsAutoconso.map(row => row.query(By.css('.mat-column-nomInstallation')).nativeElement.innerText);
    const nomInstallAutoconsoExpected = tableauSiteComponent.sites
      .filter(site => !!site.isAutoconso)
      .map(site => site.nomInstallation);
    expect(nomsInstallAutoconso).toEqual(nomInstallAutoconsoExpected);
  });

});
