import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Site } from '../../models/site.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-tableau-site',
  templateUrl: './tableau-site.component.html',
  styleUrls: ['./tableau-site.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableauSiteComponent implements OnInit {

  @Input()
  sites: Array<Site> = [];

  @Input()
  titre: string;

  displayedColumns: Array<string> = ['refContrat', 'idc', 'isAutoconso', 'nomInstallation', 'commClient', 'commUser', 'isResolu'];

  dataSource: MatTableDataSource<Site>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor() { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<Site>(this.sites);
    this.dataSource.paginator = this.paginator;
  }

}
