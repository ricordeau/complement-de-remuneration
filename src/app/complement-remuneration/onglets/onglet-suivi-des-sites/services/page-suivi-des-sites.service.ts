import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SiteStat } from '../../../../shared/models/site-stat.model';
import { Site } from '../models/site.model';
import { SITE_A_SUIVRE_STUB, SITE_STATS_PAGE_SUIVI_STUB } from '../stubs/suivi-des-sites.stub';

@Injectable({
  providedIn: 'root'
})
export class PageSuiviDesSitesService {

  constructor() { }

  getSitesStats(): Observable<Array<SiteStat>> {
    return of(SITE_STATS_PAGE_SUIVI_STUB);
  }

  getSitesASuivre(): Observable<Array<Site>> {
    return of(SITE_A_SUIVRE_STUB);
  }

}
