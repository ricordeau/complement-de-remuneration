import { TestBed } from '@angular/core/testing';

import { PageSuiviDesSitesService } from './page-suivi-des-sites.service';

describe('PageSuiviDesSitesService', () => {
  let service: PageSuiviDesSitesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PageSuiviDesSitesService);
  });

  it('L\'élément est testable', () => {
    expect(service).toBeTruthy();
  });
});
