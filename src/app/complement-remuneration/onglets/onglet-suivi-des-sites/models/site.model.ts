export class Site {
  refContrat: string;
  idc: string;
  isAutoconso: boolean;
  nomInstallation: string;
  commClient: string;
  commUser: string;
  isResolu: boolean;
}
