import { NgModule } from '@angular/core';
import { PageSuiviDesSitesComponent } from './pages/page-suivi-des-sites/page-suivi-des-sites.component';
import { SharedModule } from '../../../shared/shared.module';
import { TableauSiteComponent } from './components/tableau-site/tableau-site.component';


@NgModule({
  declarations: [
    PageSuiviDesSitesComponent,
    TableauSiteComponent
  ],
  imports: [SharedModule]
})
export class OngletSuiviDesSitesModule { }
