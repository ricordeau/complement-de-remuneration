import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-complement-remuneration',
  templateUrl: './complement-remuneration.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComplementRemunerationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
