import { MatPaginatorIntl } from '@angular/material/paginator';

export function CustomFrPaginator(): MatPaginatorIntl {
  const customPaginatorIntl = new MatPaginatorIntl();

  customPaginatorIntl.itemsPerPageLabel = 'Résultats par page ';
  customPaginatorIntl.nextPageLabel = 'Page suivante';
  customPaginatorIntl.previousPageLabel = 'Page précédente';
  customPaginatorIntl.firstPageLabel = 'Première page';
  customPaginatorIntl.lastPageLabel = 'Dernière page';
  customPaginatorIntl.getRangeLabel = (page: number, pageSize: number, length: number) => {
    return (page + 1) + '/' + Math.trunc((length / pageSize) + 1);
  };

  return customPaginatorIntl;
}
