import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagePublicationsComponent } from '../../complement-remuneration/onglets/onglet-publications/pages/page-publications/page-publications.component';
import { PageSuiviDesSitesComponent } from '../../complement-remuneration/onglets/onglet-suivi-des-sites/pages/page-suivi-des-sites/page-suivi-des-sites.component';
import { PageDetailPublicationComponent } from '../../complement-remuneration/onglets/onglet-publications/pages/page-detail-publication/page-detail-publication.component';
import { PageDetailCourbesComponent } from '../../complement-remuneration/onglets/onglet-publications/pages/page-detail-courbes/page-detail-courbes.component';

export const routes: Routes = [
  { path: 'publications', component: PagePublicationsComponent },
  { path: 'publications/detail/:id', component: PageDetailPublicationComponent },
  { path: 'publications/detail-courbes/:id', component: PageDetailCourbesComponent },
  { path: 'suivi-des-sites', component: PageSuiviDesSitesComponent },
  { path: '',   redirectTo: 'publications', pathMatch: 'full' },
  { path: '**', redirectTo: 'publications', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ComplementRemunerationRoutingModule { }
