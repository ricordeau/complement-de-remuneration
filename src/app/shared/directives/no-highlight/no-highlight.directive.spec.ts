import { NoHighlightDirective } from './no-highlight.directive';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: `
    <div appNoHighlight>
      TEST
    </div>
  `
})
class TestComponent { }

describe('NoHighlightDirective', () => {

  let testComponent: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ TestComponent, NoHighlightDirective ]
    });
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(testComponent).toBeTruthy();
  });

  it('Doit interdire le highlight sur les composants portant cette directive', waitForAsync(() => {
    fixture.whenStable().then(() => {
      const divTest = fixture.debugElement.query(By.css('div'));
      expect(divTest.nativeElement.style.userSelect).toEqual('none');
    });
  }));

});
