import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appNoHighlight]'
})
export class NoHighlightDirective {

  constructor(el: ElementRef) {
    el.nativeElement.style.userSelect = 'none';
  }

}
