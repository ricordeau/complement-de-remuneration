import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { NoHighlightDirective } from './directives/no-highlight/no-highlight.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SiteStatCadreComponent } from './components/site-stat-cadre/site-stat-cadre.component';
import { AdaptSiteStatLabelSizeDirective } from './components/site-stat-cadre/adapt-site-stat-label-size.directive';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { CustomFrPaginator } from './models/mat-paginator-custom.model';
import { TiretIfNullPipe } from './pipes/tiret-if-null.pipe';
import { MenuPrincipalComponent } from './components/menu-principal/menu-principal.component';
import { ComplementRemunerationRoutingModule } from './routes/complement-remuneration-routing.module';
import { FormsModule } from '@angular/forms';
import { CommentaireComponent } from './components/commentaire/commentaire.component';

@NgModule({
  declarations: [
    MenuPrincipalComponent,
    SiteStatCadreComponent,
    CommentaireComponent,
    NoHighlightDirective,
    AdaptSiteStatLabelSizeDirective,
    TiretIfNullPipe
  ],
  imports: [
    BrowserAnimationsModule,
    ComplementRemunerationRoutingModule,
    MatIconModule
  ],
  exports: [
    // Shared Elements
    MenuPrincipalComponent,
    SiteStatCadreComponent,
    CommentaireComponent,
    NoHighlightDirective,
    TiretIfNullPipe,
    ComplementRemunerationRoutingModule,
    // Angular
    BrowserAnimationsModule,
    FormsModule,
    // Material
    MatToolbarModule,
    MatIconModule,
    MatRippleModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule
  ],
  providers: [
    { provide: MatPaginatorIntl, useValue: CustomFrPaginator() }
  ]
})
export class SharedModule {
}
