import { TiretIfNullPipe } from './tiret-if-null.pipe';

describe('TiretIfNullPipe', () => {

  const tiretIfNullPipe = new TiretIfNullPipe();

  it('L\'élément est testable', () => {
    expect(tiretIfNullPipe).toBeTruthy();
  });

  it('Renvoie la string d\'entrée si non null', () => {
    const stringNonNull = 'stringNonNull';
    expect(tiretIfNullPipe.transform(stringNonNull)).toEqual(stringNonNull);
  });

  it('Renvoie un tiret si string null ou vide', () => {
    const stringNull = null;
    const stringVide = '';
    expect(tiretIfNullPipe.transform(stringNull)).toEqual('-');
    expect(tiretIfNullPipe.transform(stringVide)).toEqual('-');
  });

});
