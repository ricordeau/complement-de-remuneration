import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tiretIfNull'
})
export class TiretIfNullPipe implements PipeTransform {

  transform(value: any): string {
    return !!value ? value : '-';
  }

}
