import { ChangeDetectionStrategy, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';

@Component({
  selector: 'app-commentaire',
  templateUrl: './commentaire.component.html',
  styleUrls: ['./commentaire.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentaireComponent implements OnInit, OnDestroy {

  @Input() originalComment;

  @Input() idComment: string;

  commentChanged$: BehaviorSubject<string>;

  isNewCommentStarted = false;

  @ViewChild('divComment', { static: false }) divComment: ElementRef;

  constructor() {
  }

  ngOnInit(): void {
    this.originalComment = this.originalComment ? this.originalComment : '';
    this.commentChanged$ = new BehaviorSubject<string>(this.originalComment);
    this.commentChanged$.pipe(
      filter(nouveauCommentaire => !!nouveauCommentaire),
      debounceTime(3000)
    ).subscribe(nouveauCommentaire => {
      this.saveComment(nouveauCommentaire);
      this.divComment?.nativeElement.blur();
    });
  }

  ngOnDestroy(): void {
    this.commentChanged$.unsubscribe();
  }

  keyPressed(event: KeyboardEvent): void {
    this.activeDebounceOnText();
    if (event.key === 'Enter') {
      this.saveComment(this.divComment.nativeElement.innerText);
      this.divComment.nativeElement.blur();
    }
  }

  activeDebounceOnText(): void {
    this.commentChanged$.next(this.divComment.nativeElement.innerText);
  }

  startNewComment(): void {
    this.isNewCommentStarted = true;
    setTimeout(() => this.divComment.nativeElement.focus());
  }

  blur(): void {
    this.saveComment(this.divComment.nativeElement.innerText);
    this.isNewCommentStarted = false;
  }

  saveComment(newComment: string): void {
    if (newComment.trim() !== this.originalComment && this.divComment) {
      this.originalComment = newComment.trim();
      this.divComment.nativeElement.innerText = this.originalComment;
      console.log(`Commentaire à sauvegarder (${this.idComment}) : ${this.originalComment}`);
    }
  }

}
