import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { CommentaireComponent } from './commentaire.component';
import { By } from '@angular/platform-browser';

describe('CommentaireComponent', () => {
  let composantCommentaire: CommentaireComponent;
  let fixture: ComponentFixture<CommentaireComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CommentaireComponent]
    });

    fixture = TestBed.createComponent(CommentaireComponent);
    composantCommentaire = fixture.componentInstance;
    spyOn(console, 'log').and.stub();
  });

  it('L\'élément est testable', () => {
    fixture.detectChanges();
    expect(composantCommentaire).toBeTruthy();
  });

  it('Doit afficher \'Laisser un commentaire\' si le commentaire est vide', () => {
    // given
    composantCommentaire.originalComment = '';

    // when
    fixture.detectChanges();

    // then
    const divMessageDefault = fixture.debugElement.query(By.css('.default-comment'));
    expect(divMessageDefault.nativeElement.innerText).toEqual('Laisser un commentaire');

    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    expect(divMessageUtilisateur).toBeNull();
  });

  it('Doit afficher le commentaire passé en input s\'il est non vide', () => {
    // given
    composantCommentaire.originalComment = 'Commentaire non vide';

    // when
    fixture.detectChanges();

    // then
    const divMessageDefault = fixture.debugElement.query(By.css('.default-comment'));
    expect(divMessageDefault).toBeNull();

    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    expect(divMessageUtilisateur.nativeElement.innerText).toEqual('Commentaire non vide');
  });

  it('Doit sauvegarder le commentaire lorsque l\'utilisateur appuie sur la touche ENTRER', () => {
    // given
    composantCommentaire.originalComment = 'Commentaire saisi';
    const saveCommSpy = spyOn(composantCommentaire, 'saveComment');

    // when
    fixture.detectChanges();
    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    divMessageUtilisateur.triggerEventHandler('keydown', new KeyboardEvent('KeyboardEvent', {key: 'Enter'}));

    // then
    expect(saveCommSpy).toHaveBeenCalledWith('Commentaire saisi');
  });

  it('Ne doit pas sauvegarder le commentaire lorsque l\'utilisateur appuie sur une autre touche qu\' ENTRER', () => {
    // given
    composantCommentaire.originalComment = 'Commentaire saisi';
    const saveCommSpy = spyOn(composantCommentaire, 'saveComment');

    // when
    fixture.detectChanges();
    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    divMessageUtilisateur.triggerEventHandler('keydown', new KeyboardEvent('KeyboardEvent', {key: 'T'}));

    // then
    expect(saveCommSpy).toHaveBeenCalledTimes(0);
  });

  it('Doit sauvegarder lors d\'un événement blur', () => {
    // given
    composantCommentaire.originalComment = 'Commentaire original';

    // when
    fixture.detectChanges();
    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    divMessageUtilisateur.nativeElement.innerText = 'Commentaire modifié';
    divMessageUtilisateur.triggerEventHandler('blur', null);

    // then
    expect(composantCommentaire.originalComment).toEqual('Commentaire modifié');
  });

  it('Ne doit pas sauvegarder le commentaire s\'il est identique au précédent', () => {
    // given
    composantCommentaire.originalComment = 'Commentaire original';

    // when
    fixture.detectChanges();
    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    divMessageUtilisateur.nativeElement.innerText = 'Commentaire original';
    divMessageUtilisateur.triggerEventHandler('blur', null);
    fixture.detectChanges();

    // then
    expect(console.log).toHaveBeenCalledTimes(0);
  });

  it('Doit sauvegarder le commentaire modifié au bout de 3 secondes', fakeAsync(() => {
    // given
    composantCommentaire.originalComment = 'Commentaire original';

    // when
    fixture.detectChanges();
    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    divMessageUtilisateur.nativeElement.innerText = 'Commentaire modifié';
    divMessageUtilisateur.triggerEventHandler('input', null);

    // then
    expect(composantCommentaire.originalComment).toEqual('Commentaire original');
    tick(3000);
    expect(composantCommentaire.originalComment).toEqual('Commentaire modifié');
  }));

  it('Doit afficher la div éditable lorsque l\'utilisateur clique sur la div par défaut', () => {
    // given
    composantCommentaire.originalComment = '';

    // when
    fixture.detectChanges();
    let divMessageDefault = fixture.debugElement.query(By.css('.default-comment'));
    expect(composantCommentaire.isNewCommentStarted).toBeFalse();
    divMessageDefault.triggerEventHandler('click', null);
    fixture.detectChanges();

    // then
    expect(composantCommentaire.isNewCommentStarted).toBeTrue();
    const divMessageUtilisateur = fixture.debugElement.query(By.css('.comment-container'));
    divMessageDefault = fixture.debugElement.query(By.css('.default-comment'));
    expect(divMessageUtilisateur).toBeDefined();
    expect(divMessageDefault).toBeNull();
  });

});
