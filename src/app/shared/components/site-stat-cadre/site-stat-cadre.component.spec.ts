import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteStatCadreComponent } from './site-stat-cadre.component';
import { AdaptSiteStatLabelSizeDirective } from './adapt-site-stat-label-size.directive';
import { By } from '@angular/platform-browser';

describe('SiteStatCadreComponent', () => {
  let siteStatCadreComponent: SiteStatCadreComponent;
  let fixture: ComponentFixture<SiteStatCadreComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteStatCadreComponent, AdaptSiteStatLabelSizeDirective ]
    });
    fixture = TestBed.createComponent(SiteStatCadreComponent);
    siteStatCadreComponent = fixture.componentInstance;
    siteStatCadreComponent.siteStat = { value: '10', description: 'Test stat cadre' };
    siteStatCadreComponent.width = '253px';
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(siteStatCadreComponent).toBeTruthy();
  });

  it('La largeur du cadre prend en compte l\'input', () => {
    const cadre = fixture.debugElement.query(By.css('.stat-site-cadre-container'));
    expect(cadre.nativeElement.style.width).toEqual('253px');
  });

  it('La valeur de la stat est affichée ainsi que son détail', () => {
    const valeurStatSpan = fixture.debugElement.query(By.css('.value'));
    const detailStatSpan = fixture.debugElement.query(By.css('.description'));
    expect(valeurStatSpan.nativeElement.innerText).toEqual('10');
    expect(detailStatSpan.nativeElement.innerText).toEqual('Test stat cadre');
  });

});
