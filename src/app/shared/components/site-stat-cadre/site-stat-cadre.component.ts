import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { SiteStat } from '../../models/site-stat.model';

@Component({
  selector: 'app-site-stat-cadre',
  templateUrl: './site-stat-cadre.component.html',
  styleUrls: ['./site-stat-cadre.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SiteStatCadreComponent implements OnInit {

  @Input()
  siteStat: SiteStat;

  @Input()
  width = '200px';

  constructor() { }

  ngOnInit(): void {
  }

}
