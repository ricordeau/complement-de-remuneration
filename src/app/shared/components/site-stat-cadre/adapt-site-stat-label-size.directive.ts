import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAdaptSiteStatLabelSize]'
})
export class AdaptSiteStatLabelSizeDirective {

  constructor(el: ElementRef) {
    setTimeout(() => {
      if (el.nativeElement.offsetHeight > 30) {
        el.nativeElement.style.fontSize = '12px';
      }
    });
  }

}
