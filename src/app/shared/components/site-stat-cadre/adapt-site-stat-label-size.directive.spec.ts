import { AdaptSiteStatLabelSizeDirective } from './adapt-site-stat-label-size.directive';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: `
    <div appAdaptSiteStatLabelSize
         style="height: 40px; width: 40px; font-size: 50px; border: solid 1px">
      TEST
    </div>
  `
})
class TestComponent { }

describe('AdaptSiteStatLabelSizeDirective', () => {

  let testComponent: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ TestComponent, AdaptSiteStatLabelSizeDirective ]
    });
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('L\'élément est testable', () => {
    expect(testComponent).toBeTruthy();
  });

  it('La taille du texte est mise à 12px si la hauteur de la div est supérieur à 30px',  waitForAsync(() => {
    fixture.whenStable().then(() => {
      const divTest = fixture.debugElement.query(By.css('div'));
      expect(divTest.nativeElement.style.fontSize).toEqual('12px');
    });
  }));

  it('La taille du texte n\'est pas modifiée si la hauteur de la div est inférieure à 30px', waitForAsync(() => {
    const divTest = fixture.debugElement.query(By.css('div'));
    divTest.nativeElement.style.height = '20px';
    fixture.whenStable().then(() => {
      expect(divTest.nativeElement.style.fontSize).toEqual('50px');
    });
  }));

});
