import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DEFAULT_MENU_TABS, MenuTab } from './menu-principal.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-principal',
  templateUrl: './menu-principal.component.html',
  styleUrls: ['./menu-principal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuPrincipalComponent implements OnInit {

  menuTabs: Array<MenuTab> = [];

  activatedTab: MenuTab;

  constructor(private router: Router, private cd: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    setTimeout(() => {
      const activeUrl = this.router.url;
      const activeTabName = activeUrl.includes('publications') ? 'PUBLICATIONS' : 'SUIVI DES SITES';
      this.changeActiveTab(activeTabName);
    });
  }

  changeActiveTab(tabName: string): void {
    const menuTabsUpdated: Array<MenuTab> = DEFAULT_MENU_TABS;
    menuTabsUpdated.forEach(tab => tab.isActive = false);
    this.activatedTab = menuTabsUpdated.find(tab => tab.name === tabName);
    this.activatedTab.isActive = true;
    this.menuTabs = menuTabsUpdated;
    this.cd.markForCheck();
  }

}
