export class MenuTab {
  name: string;
  routeUrl: string;
  isActive: boolean;
}

export const DEFAULT_MENU_TABS: Array<MenuTab> = [
  { name: 'PUBLICATIONS', routeUrl: '/publications', isActive: true },
  { name: 'SUIVI DES SITES', routeUrl: '/suivi-des-sites', isActive: false }
];
