import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { MenuPrincipalComponent } from './menu-principal.component';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { routes } from '../../routes/complement-remuneration-routing.module';
import { DEFAULT_MENU_TABS } from './menu-principal.model';
import { By } from '@angular/platform-browser';

describe('MenuPrincipalComponent', () => {
  let menuPrincipalComponent: MenuPrincipalComponent;
  let fixture: ComponentFixture<MenuPrincipalComponent>;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPrincipalComponent ],
      imports: [ RouterTestingModule.withRoutes(routes), MatToolbarModule, MatIconModule ]
    });
    fixture = TestBed.createComponent(MenuPrincipalComponent);
    menuPrincipalComponent = fixture.componentInstance;
    menuPrincipalComponent.menuTabs = DEFAULT_MENU_TABS;
    router = TestBed.inject(Router);
  });

  it('L\'élément est testable', () => {
    expect(menuPrincipalComponent).toBeTruthy();
  });

  it('Sélectionne l\'onglet publications si la route contient \'publications\'', fakeAsync(() => {
    fixture.ngZone.run(() => {
      // given
      router.navigateByUrl('/publications');

      // when
      fixture.detectChanges();
      tick();

      // then
      fixture.detectChanges();
      expect(menuPrincipalComponent.activatedTab.name).toEqual('PUBLICATIONS');
      const divsTabsArray = fixture.debugElement.queryAll(By.css('.menu-button'));
      const selectedTab = divsTabsArray.find(tab => tab.nativeElement.classList.value.includes('selected')).nativeElement;
      expect(selectedTab.innerText).toEqual('PUBLICATIONS');
    });
  }));

  it('Sélectionne l\'onglet suivi des sites si la route ne contient pas \'publications\'', fakeAsync(() => {
    fixture.ngZone.run(() => {
      // given
      router.navigateByUrl('/suivi-des-sites');

      // when
      fixture.detectChanges();
      tick();

      // then
      fixture.detectChanges();
      expect(menuPrincipalComponent.activatedTab.name).toEqual('SUIVI DES SITES');
      const divsTabsArray = fixture.debugElement.queryAll(By.css('.menu-button'));
      const selectedTab = divsTabsArray.find(tab => tab.nativeElement.classList.value.includes('selected')).nativeElement;
      expect(selectedTab.innerText).toEqual('SUIVI DES SITES');
    });
  }));

  it('Navigue vers la page publication lors d\'un clique sur l\'onglet publications', fakeAsync(() => {
    fixture.ngZone.run(() => {
      // given
      router.navigateByUrl('/suivi-des-sites');

      // when
      fixture.detectChanges();
      tick();

      // then
      fixture.detectChanges();
      const divsTabsArray = fixture.debugElement.queryAll(By.css('.menu-button'));
      const selectedTab = divsTabsArray.find(tab => tab.nativeElement.innerText.includes('PUBLICATIONS'));
      expect(router.url).toEqual('/suivi-des-sites');
      selectedTab.triggerEventHandler('click', null);
      tick();
      expect(router.url).toEqual('/publications');
    });
  }));

  it('Navigue vers la page suivi des sites lors d\'un clique sur l\'onglet suivi des sites', fakeAsync(() => {
    fixture.ngZone.run(() => {
      // given
      router.navigateByUrl('/publications');

      // when
      fixture.detectChanges();
      tick();

      // then
      fixture.detectChanges();
      const divsTabsArray = fixture.debugElement.queryAll(By.css('.menu-button'));
      const selectedTab = divsTabsArray.find(tab => tab.nativeElement.innerText.includes('SUIVI DES SITES'));
      expect(router.url).toEqual('/publications');
      selectedTab.triggerEventHandler('click', null);
      tick();
      expect(router.url).toEqual('/suivi-des-sites');
    });
  }));

});
