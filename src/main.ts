import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ComplementRemunerationModule } from './app/complement-remuneration/complement-remuneration.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(ComplementRemunerationModule)
  .catch(err => console.error(err));
